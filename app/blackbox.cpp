#include <iostream>

#include "version.h"

#include "mymath/mymath.h"

using namespace std;

int main()
{
    std::cout << "Hello blackbox v" << MAJOR_VERSION << "." << MINOR_VERSION << "." << PATCH_VERSION << std::endl;

    std::cout << "math add 10 + 10 = " << mymath::add(10, 10) << std::endl;
    
    return 0;
};
