#include <iostream>

#include "version.h"

using namespace std;

int main()
{
    std::cout << "Hello whitebox v" << MAJOR_VERSION << "." << MINOR_VERSION << "." << PATCH_VERSION << std::endl;
    return 0;
};
