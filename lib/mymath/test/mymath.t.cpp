#include "catch2.h"
#include "mymath/mymath.h"

TEST_CASE( "mymath add" )
{
    REQUIRE(mymath::add(100,43) == 143);
}